import React from "react";
import { withStyles, TextField, Button } from "@material-ui/core";
import styles from "./HomeStyles";

function HomeContainer(props) {
  const { classes } = props;
  return (
    <div className={props.classes.root}>
      <div className={classes.searchContainer}>
        <TextField id="standard-search" label="Search field" type="search" margin="normal" className={classes.textField} />
        <Button variant="contained" color="primary" className={classes.button}>
          Primary
        </Button>
      </div>
    </div>
  );
}

export default withStyles(styles)(HomeContainer);
