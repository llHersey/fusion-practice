import { createStyles } from "@material-ui/core";

const styles = theme => {
  const unit = theme.spacing.unit;
  return createStyles({
    root: {
      margin: unit * 3,
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
    textField: {
      width: 400,
    },
    searchContainer: {
      display: "flex",
      justifyContent: "center",
      alignItems: "flex-end",
    },
    button: {
      margin: theme.spacing.unit,
      height: unit * 5,
    },
  });
};

export default styles;
