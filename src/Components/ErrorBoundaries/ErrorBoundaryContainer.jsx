import React, { Component } from "react";

class ErrorBoundary extends Component {
  state = {};

  componentDidCatch(error, errorInfo) {
    this.setState({ error });
    //logService.log(error, errorInfo);
  }

  render() {
    return this.state.error ? <div>Error</div> : this.props.children;
  }
}

export default ErrorBoundary;
