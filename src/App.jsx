import React from "react";
import { ToastContainer } from "react-toastify";
import { MuiThemeProvider as ThemeProvider } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import ErrorBoundary from "./Components/ErrorBoundaries/ErrorBoundaryContainer";
import MainTheme from "./Themes/MainTheme";
import Layout from "./Components/Layout/LayoutComponent";
import { Switch, Route } from "react-router-dom";
import HomeContainer from "./Modules/Home/HomeContainer";

function App() {
  return (
    <React.Fragment>
      <ToastContainer />
      <ErrorBoundary>
        <CssBaseline />
        <ThemeProvider theme={MainTheme}>
          <Layout />
          <Switch>
            <Route exact={true} path="/" component={HomeContainer} />
          </Switch>
        </ThemeProvider>
      </ErrorBoundary>
    </React.Fragment>
  );
}

export default App;
