import { createMuiTheme } from "@material-ui/core/styles";

const MainTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#2196f3",
    },
    secondary: {
      main: "#e10050",
    },
  },
  typography: {
    useNextVariants: true,
  },
});

export default MainTheme;
